Simple and fast modular autorig for maya.  
The tool is originally made for Project Borealis' production needs but is meant to be usable for any production with little to no modifications.